package ru.javazen.talk.api.auth;

import java.util.Base64;

/**
 * User credentials for authentication
 */
public class UserCredentials {

    private String username;
    private String password;
    private String basic;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBasic() {
        if (basic == null) {
            String forBasic = username + ":" + password;
            basic = "Basic " + Base64.getEncoder().encodeToString(forBasic.getBytes());
        }
        return basic;
    }

    public void setBasic(String basic) {
        this.basic = basic;
    }
}
