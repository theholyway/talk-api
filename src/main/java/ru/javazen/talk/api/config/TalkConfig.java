package ru.javazen.talk.api.config;

import com.sun.jersey.api.client.AsyncWebResource;
import com.sun.jersey.api.client.WebResource;
import ru.javazen.talk.api.auth.UserCredentials;

public interface TalkConfig {

    WebResource getWebResource();

    UserCredentials getCredentials();

    AsyncWebResource getAsyncWebResource();
}
