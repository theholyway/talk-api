package ru.javazen.talk.api.config;

import com.sun.jersey.api.client.AsyncWebResource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.javazen.talk.api.auth.UserCredentials;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Properties;


public class DefaultTalkConfig implements TalkConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultTalkConfig.class);

    private static final String DEFAULT_BE = "http://javazen.ru/talk-be/";
    private static final String API_PATH = "api";
    private static final String CONFIG_PROPERTIES = "talk-config.properties";
    private static final String BACKEND_URL_PROP = "backend.url";
    private static final String USERNAME_PROP = "auth.username";
    private static final String PASSWORD_PROP = "auth.password";

    private WebResource webResource;
    private AsyncWebResource asyncWebResource;
    private Properties properties;

    public DefaultTalkConfig() {
        properties = loadProperties();
        webResource = webResource();
        asyncWebResource = asyncWebResource();
    }

    @Override
    public WebResource getWebResource() {
        return webResource;
    }

    @Override
    public AsyncWebResource getAsyncWebResource() {
        return asyncWebResource;
    }

    @Override
    public UserCredentials getCredentials() {
        if (properties.isEmpty()) {
            return null;
        }
        UserCredentials credentials = new UserCredentials();
        credentials.setUsername(properties.getProperty(USERNAME_PROP));
        credentials.setPassword(properties.getProperty(PASSWORD_PROP));

        String forBasic = credentials.getUsername() + ":" + credentials.getPassword();
        String basic = "Basic " + Base64.getEncoder().encodeToString(forBasic.getBytes());
        credentials.setBasic(basic);

        return credentials;
    }

    private Client client(){
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getClasses().add(JacksonJsonProvider.class);
        return Client.create(clientConfig);
    }

    private WebResource webResource(){
        String beUrl = properties.containsKey(BACKEND_URL_PROP) ? properties.getProperty(BACKEND_URL_PROP) : DEFAULT_BE;
        return client().resource(beUrl).path(API_PATH);
    }

    private AsyncWebResource asyncWebResource() {
        String beUrl = properties.containsKey(BACKEND_URL_PROP) ? properties.getProperty(BACKEND_URL_PROP) : DEFAULT_BE;
        return client().asyncResource(beUrl).path(API_PATH);
    }

    private Properties loadProperties() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(CONFIG_PROPERTIES);
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            LOGGER.error("Cannot load properties for talk api ({})", CONFIG_PROPERTIES, e);
        }
        return properties;
    }
}