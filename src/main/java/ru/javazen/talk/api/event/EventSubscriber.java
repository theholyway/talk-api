package ru.javazen.talk.api.event;

/**
 * Created by Andrew on 27.05.2017.
 */
public interface EventSubscriber<T> {

    void onEvent(T entity);
}
