package ru.javazen.talk.api.event;

import ru.javazen.talk.api.entity.ChatUpdateResponse;

public interface ChatUpdateSubscriber extends EventSubscriber<ChatUpdateResponse> { }
