package ru.javazen.talk.api.service;

import ru.javazen.talk.api.entity.Chat;
import ru.javazen.talk.api.entity.ChatUpdate;
import ru.javazen.talk.api.entity.KnowledgeBase;
import ru.javazen.talk.api.entity.User;
import ru.javazen.talk.api.event.ChatUpdateSubscriber;

import java.util.List;

/**
 * Provide methods for sent messages and subscribing on updates
 */
public interface TalkService {

    /**
     * Sent message to chat
     * @param update chat update
     */
    void sendUpdate(ChatUpdate update);

    /**
     * Subscribe for updates from chats
     * @param subscriber subscriber for update processing
     */
    void onMessage(ChatUpdateSubscriber subscriber);

    /**
     * Subscribe for updates from chats
     * @param subscriber subscriber for update processing
     * @param includeCurrentUser determines whether the current user should receive updates
     */
    void onMessage(ChatUpdateSubscriber subscriber, boolean includeCurrentUser);

    /**
     * Returns all chats by Knowledge Base id for current user
     * @param knowledgeBaseId Knowledge Base id
     * @return list of chats
     */
    List<Chat> getChatsByKnowledgeBase(Long knowledgeBaseId);

    /**
     * Returns all chats for current user
     * @return list of chats
     */
    List<Chat> getAllChats();

    /**
     * Returns all available knowledge bases in system for current user
     * @return list of knowledge bases
     */
    List<KnowledgeBase> getAllKnowledgeBases();

    /**
     * Creates new chat for Knowledge Base user and current user
     * @param title title of new chat
     * @param knowledgeBaseId Knowledge Base id
     * @return created chat
     */
    Chat createChatByKnowledgeBaseId(String title, Long knowledgeBaseId);

    /**
     * Reset context for chat
     * @param chatId chat id
     */
    void resetChatContext(Long chatId);

    /**
     * Start long polling service
     */
    void startLongPolling();

    /**
     * Stop long polling service
     */
    void stopLongPolling();

    /**
     * Returns current user
     * @return current user
     */
    User getCurrentUser();
}
