package ru.javazen.talk.api.service.impl;

import com.sun.jersey.api.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.javazen.talk.api.auth.UserCredentials;
import ru.javazen.talk.api.config.TalkConfig;
import ru.javazen.talk.api.entity.*;
import ru.javazen.talk.api.event.ChatUpdateSubscriber;
import ru.javazen.talk.api.service.TalkService;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;


public class TalkServiceImpl implements TalkService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TalkServiceImpl.class);

    private TalkConfig config;

    private WebResource apiWebResource;
    private AsyncWebResource asyncWebResource;
    private UserCredentials credentials;

    //private List<ChatUpdateSubscriber> chatUpdateSubscribers = new ArrayList<>();
    private Map<ChatUpdateSubscriber, Boolean> chatUpdateSubscribers = new HashMap<>();
    private Long currentUserId;
    private long lastUpdate = -1;
    private Thread thread;

    private Long getCurrentUserId() {
        if (currentUserId == null) {
            User user = getCurrentUser();
            if(user != null) {
                currentUserId = getCurrentUser().getId();
            }
        }
        return currentUserId;
    }
    public TalkServiceImpl(TalkConfig config) {
        this.config = config;
        apiWebResource = config.getWebResource();
        asyncWebResource  = config.getAsyncWebResource();
        credentials = config.getCredentials();
        currentUserId = getCurrentUser().getId();
    }

    @Override
    public void sendUpdate(ChatUpdate update) {
        WebResource.Builder sendUpdate = apiWebResource
                .path("chat")
                .path("update")
                .path("send")
                .header("Authorization", credentials.getBasic());

        ClientResponse response = sendUpdate
                .type(MediaType.APPLICATION_JSON_TYPE)
                .post(ClientResponse.class, update);

        if (response.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            LOGGER.debug("Message sent successfully");
        } else {
            String details = response.getEntity(String.class);
            LOGGER.error("Error while sending message, status code: {}, details: {}", response.getStatus(), details);
        }
    }

    @Override
    public void onMessage(ChatUpdateSubscriber subscriber) {
        onMessage(subscriber, false);
    }

    @Override
    public void onMessage(ChatUpdateSubscriber subscriber, boolean includeCurrentUser) {
        chatUpdateSubscribers.put(subscriber, includeCurrentUser);
    }

    @Override
    public void stopLongPolling() {
        if (thread != null) {
            LOGGER.info("Stop LongPulling service");
            thread.interrupt();
        }
    }

    @Override
    public void startLongPolling() {
        if (thread != null && !thread.isInterrupted()) { return; }
        LOGGER.info("Start LongPulling service");

        Runnable runnable = () -> {
            while (!Thread.currentThread().isInterrupted()) {
                longPollingUpdater();
            }
            LOGGER.info("Shutdown LongPulling service");
        };

        thread = new Thread(runnable);
        thread.start();
    }

    @Override
    public List<Chat> getChatsByKnowledgeBase(Long knowledgeBaseId) {
        WebResource.Builder sendUpdate = apiWebResource
                .path("chat")
                .path("get")
                .path("kb" + knowledgeBaseId)
                .header("Authorization", credentials.getBasic());

        ClientResponse response = sendUpdate
                .type(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class);

        if (response.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            List<Chat> chats = response.getEntity(new GenericType<List<Chat>>(){});
            LOGGER.debug("Obtained chats count: {}, knowledge base: {}", chats.size(), knowledgeBaseId);
            return chats;
        } else {
            String details = response.getEntity(String.class);
            LOGGER.error("Error during get chats for kb: {}, status code: {}, details: {}", knowledgeBaseId, response.getStatus(), details);
        }
        return null;
    }

    @Override
    public List<Chat> getAllChats() {
        WebResource.Builder sendUpdate = apiWebResource
                .path("chat")
                .path("get")
                .path("all")
                .header("Authorization", credentials.getBasic());

        ClientResponse response = sendUpdate
                .type(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class);

        if (response.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            List<Chat> chats = response.getEntity(new GenericType<List<Chat>>(){});
            LOGGER.debug("Obtained chats count: {}", chats.size());
            return chats;
        } else {
            String details = response.getEntity(String.class);
            LOGGER.error("Error during get all chats, status code: {}, details: {}", response.getStatus(), details);
        }
        return null;
    }

    @Override
    public List<KnowledgeBase> getAllKnowledgeBases() {
        WebResource.Builder sendUpdate = apiWebResource
                .path("knowledge_base")
                .path("get")
                .path("all")
                .header("Authorization", credentials.getBasic());

        ClientResponse response = sendUpdate
                .type(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class);

        if (response.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            List<KnowledgeBase> knowledgeBases = response.getEntity(new GenericType<List<KnowledgeBase>>(){});
            LOGGER.debug("Obtained knowledge bases count: {}", knowledgeBases.size());
            return knowledgeBases;
        } else {
            String details = response.getEntity(String.class);
            LOGGER.error("Error during get all knowledge bases, status code: {}, details: {}", response.getStatus(), details);
        }
        return null;
    }

    @Override
    public Chat createChatByKnowledgeBaseId(String title, Long knowledgeBaseId) {
        WebResource.Builder sendUpdate = apiWebResource
                .path("chat")
                .path("create")
                .path("kb" + knowledgeBaseId)
                .header("Authorization", credentials.getBasic());

        Chat chat = new Chat();
        chat.setTitle(title);

        ClientResponse response = sendUpdate
                .type(MediaType.APPLICATION_JSON_TYPE)
                .post(ClientResponse.class, chat);

        if (response.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            chat = response.getEntity(Chat.class);
            LOGGER.debug("Chat created successfully for kb: {}, new chat id: {}", knowledgeBaseId, chat.getId());
            return chat;
        } else {
            String details = response.getEntity(String.class);
            LOGGER.error("Error during creating chat for kb: {}, status code: {}, details: {}",
                    knowledgeBaseId, response.getStatus(), details);
        }
        return null;
    }

    @Override
    public void resetChatContext(Long chatId) {
        WebResource.Builder sendUpdate = apiWebResource
                .path("chat")
                .path(chatId.toString())
                .path("reset")
                .header("Authorization", credentials.getBasic());

        ClientResponse response = sendUpdate
                .type(MediaType.APPLICATION_JSON_TYPE)
                .post(ClientResponse.class);

        if (response.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            LOGGER.debug("Chat context reset successfully for: {}", chatId);
        } else {
            String details = response.getEntity(String.class);
            LOGGER.error("Error during reset context for chat: , status code: {}, details: {}", chatId, response.getStatus(), details);
        }
    }

    @Override
    public User getCurrentUser() {
        WebResource.Builder sendUpdate = apiWebResource
                .path("auth")
                .path("user")
                .header("Authorization", credentials.getBasic());

        ClientResponse response = sendUpdate
                .type(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientResponse.class);

        if (response.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            User user = response.getEntity(User.class);
            LOGGER.debug("Obtained user id: {}", user.getId());
            return user;
        } else {
            String details = response.getEntity(String.class);
            LOGGER.error("Error during get current user, status code: {}, details: {}", response.getStatus(), details);
        }
        return null;
    }

    public WebResource getApiWebResource() {
        return apiWebResource;
    }

    public void setApiWebResource(WebResource apiWebResource) {
        this.apiWebResource = apiWebResource;
    }

    private void longPollingUpdater() {

        AsyncWebResource resource = asyncWebResource
                .path("longpolling")
                .queryParam("type", "chat");

        if (lastUpdate > 0) {
            resource = resource.queryParam("from_id", Long.toString(lastUpdate));
        }

        Future<ClientResponse> responseFuture = resource
                .header("Authorization", credentials.getBasic())
                .type("application/json")
                .get(ClientResponse.class);

        try {
            ClientResponse response = responseFuture.get(40, TimeUnit.SECONDS);
            if (response.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
                List<LongPollingUpdate<ChatUpdateResponse>> longPollingUpdates
                        = response.getEntity(new GenericType<List<LongPollingUpdate<ChatUpdateResponse>>>() {});
                LOGGER.debug("The number of received updates: {}", longPollingUpdates.size());

                for (LongPollingUpdate<ChatUpdateResponse> longPollingUpdate : longPollingUpdates) {
                    LOGGER.debug("Id of the update: {}", longPollingUpdate.getId());

                    chatUpdateSubscribers.entrySet().forEach(subscriber -> {
                        if (!longPollingUpdate.getData().getUserId().equals(getCurrentUserId()) || subscriber.getValue()) {
                            subscriber.getKey().onEvent(longPollingUpdate.getData());
                        }
                    });

                    if (longPollingUpdate.getId() > lastUpdate) {
                        lastUpdate = longPollingUpdate.getId();
                    }
                }
            } else {
                String details = response.getEntity(String.class);
                LOGGER.error("Error while waiting for updates, status code: {}, details: {}", response.getStatus(), details);
            }
        } catch (TimeoutException | ExecutionException e) {
            LOGGER.error("Error during execution long polling service", e);
            if (e.getCause() instanceof UniformInterfaceException) {
                UniformInterfaceException cause = (UniformInterfaceException) e.getCause();
                String error = cause.getResponse().getEntity(String.class);
                LOGGER.error("Error details: {}", error);
            }
        } catch (InterruptedException e) {
            LOGGER.warn("Interrupt thread, long pulling service will stop", e);
            Thread.currentThread().interrupt();
        }
    }
}